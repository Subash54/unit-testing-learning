package com.example.student;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StudentValidatorTest {

    private StudentValidator studentValidator;

    @BeforeEach
    void setup() {
        studentValidator = new StudentValidator();
    }

    @Test
    public void shouldValidateThatNameShouldNotBeNull() {
        assertEquals(false, studentValidator.validateName(null));
    }

    @Test
    void validateName() {

        assertEquals(true, studentValidator.validateName("Ramkumar"));
        assertEquals(false, studentValidator.validateName("sub"));
        assertEquals(false, studentValidator.validateName("123456"));
    }

    @Test
    void validateEmail() {
        assertEquals(false, studentValidator.validateEmail("abcnkfjljaf"));
        assertEquals(true, studentValidator.validateEmail("Ramkumar@gmail.com"));
    }
}