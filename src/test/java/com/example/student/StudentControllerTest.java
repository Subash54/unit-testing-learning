package com.example.student;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class StudentControllerTest {

    @Mock
    private StudentRepository studentRepository;
    @Mock
    private StudentValidator validator;
    @Mock
    private EmailService emailService;
    @InjectMocks
    private StudentController studentController;

    @BeforeEach
    void setup() {
        initMocks(this);
    }

    @Test
    public void shouldSaveAndReturnStudent() {
        var student = new Student(0, "subash", "test@gmail.com");
        when(validator.isValid(student)).thenReturn(true);
        when(studentRepository.save(student)).thenReturn(student);

        doNothing().when(emailService).sendEmail(student);

        Student saveStudent = studentController.addStudent(student);


        assertEquals(saveStudent.getName(), student.getName());
        assertEquals(saveStudent.getEmail(), student.getEmail());


    }


}