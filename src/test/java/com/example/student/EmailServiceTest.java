package com.example.student;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

class EmailServiceTest {


    @Spy
    @Autowired
    EmailService emailService = new EmailService();

    @Captor
    ArgumentCaptor<String> argumentCaptor;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test

    public void shouldThrowExceptionWhennameIsNull() {
        String expectedMessage = "Invalid Input";
        Student student = new Student(3, null, "rohit@gmail.com");
        try {
            emailService.sendEmail(student);
        } catch (Exception e) {
            assertEquals(e.getMessage(), expectedMessage);
            return;
        }

        fail("Exception not thrown");


    }


    @Test
    public void shouldGeneratePasswordDuringSendEmail() {
        Student student = new Student(3, "rohitkumar", "rohit@gmail.com");
        emailService.sendEmail(student);
        verify(emailService, times(1)).generatePassword(argumentCaptor.capture());
        assertEquals(student.getEmail(), argumentCaptor.getValue());

    }

}