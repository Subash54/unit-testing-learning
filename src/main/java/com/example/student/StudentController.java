package com.example.student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@Service
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private StudentValidator studentValidator;
    @Autowired
    private EmailService emailService;

    @GetMapping("/getStudentById/{id}")
    @ResponseBody
    public Optional<Student> getStudentById(@PathVariable int id) {
        return studentRepository.findById(id);
    }

    @PostMapping("/addStudent")
    public Student addStudent(@RequestBody Student student) {
        //TODO: Add exception
        if (!studentValidator.isValid(student)) return null;
        Student savedStudent = studentRepository.save(student);
        emailService.sendEmail(student);
        return savedStudent;


    }
}
