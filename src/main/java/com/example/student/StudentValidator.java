package com.example.student;

import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class StudentValidator {
    public boolean isValid(Student student) {
        return validateName(student.getName()) && validateEmail(student.getEmail());
    }

    public boolean validateName(String name) {
        if (name != null) {
            String regex = "^[A-Za-z][A-Za-z]{5,29}$";

            // Compile the ReGex
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(name);
            return matcher.matches();
        }
        return false;
    }

    public boolean validateEmail(String emailId) {
        if (emailId != null) {
            String regex = "^(.+)@(.+)$";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(emailId);
            return matcher.matches();


        }
        return false;


    }
}
