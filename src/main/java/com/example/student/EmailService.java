package com.example.student;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class EmailService {


    public void sendEmail(Student student) {
        if (student.getName() == null) {
            throw new RuntimeException("Invalid Input");
        }
        generatePassword(student.getEmail());
        System.out.println("Email Send Sucessfully");

    }

    public String generatePassword(String emailId) {
        Random random = new Random();
        return emailId + random.nextInt();
    }
}
